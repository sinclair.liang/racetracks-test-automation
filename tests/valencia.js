
import Valencia from '../pageObjects/Valencia.js';
import launchPuppeteer from '../utils/launchPuppeteer';

const monzaUser = process.env.MONZA_TEST_EMAIL
const monzaPassword = process.env.MONZA_TEST_PASSWORD
let baseUrl = process.env.ENV==="PROD" ? "https://valencia.fairfaxregional.com.au/" : "https://valencia.uat2.newsnow.io/"

describe('valencia', function(){
    this.timeout(0);
    let browser
    let page
    let valencia
    let bucketName = `test${Date.now()}`

    before (async () => {
        browser = await launchPuppeteer();
        page = await browser.newPage();
        await page.setViewport({ width: 1024, height: 768});
        valencia = await new Valencia(page);
    });
    
    after (async () => {
        await browser.close()
    })

    beforeEach(async () => {
        await page.goto(baseUrl)
        await valencia.login(monzaUser, monzaPassword)
    });

    // it('should log user in', async () => {
    //     await valencia.login(monzaUser, monzaPassword)
    // })

    it('create new bucket', async () => {
        await valencia.createBucket(bucketName)
    });

    it('should upload picture', async () => {
        await valencia.uploadPicture(bucketName)
    });

    
    it('should edit picture information', async () => {
        await valencia.openRecentImage()
        await valencia.updateImageInfo(bucketName)
        await valencia.validateImageInNewsNow(monzaUser, monzaPassword, bucketName)
    });

    it('should NOT delete bucket with picture inside', async () => {
        
        await valencia.openBucket(bucketName)
        await valencia.validateDeleteBucket(false)
    });

    it('should delete bucket after deleting the picture inside', async () => {
        await valencia.deleteRecentImage()
        await valencia.openBucket(bucketName)
        await valencia.validateDeleteBucket(true)
    });

    




    // it('should upload', () => {
    //     valencia.uploadFile()
    // })
    
});
