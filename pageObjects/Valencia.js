import { expect } from 'chai';
import path from 'path'

export default class Valencia {

    constructor(page) {
        this.page = page;
    }

    login = async (username, password) => {
        if (await this.page.$('.hero-actions a:nth-child(2)') !== null){
            let loginStartButtonSelector = await this.page.$('.hero-actions a:nth-child(2)')
            await loginStartButtonSelector.click()
            await this.page.waitForSelector("#id_email")
            await this.page.type('#id_email', username, {delay: 1})
            await this.page.type('#id_password', password, {delay: 1})
            let loginButtonSelector = await this.page.$('.btn-primary')
            await loginButtonSelector.click()
            await this.page.waitForSelector(".page-header h2", {timeout: 100000})
            const pageHeaderText = await this.page.$eval(".page-header h2", element => element.textContent);
            expect(pageHeaderText).to.eql("Your Uploads");
        }

    }

    createBucket = async (bucketName) => {
        let createBucketLinkSelector = await this.page.$("#sidenav > ul > li:nth-child(5) > a")
        await createBucketLinkSelector.click()
        await this.page.waitForSelector('#id_name')
        let bucketNameSelector = await this.page.$('#id_name')
        await bucketNameSelector.type(bucketName)
        let submitBucketNameSelector = await this.page.$('.form-actions input')
        await submitBucketNameSelector.click()
        await this.page.waitForSelector(".page-header h2", {timeout: 100000})
        let bucketElements = await this.page.$$('td a')
        let bucketList = []
        for (let index = 0; index < bucketElements.length; index++) {
            const element = bucketElements[index];
            const linkText = await this.page.evaluate(el => el.innerText, element);
            bucketList.push(linkText)
        }
        // await this.page.waitFor(10000)
        expect(bucketList).to.include(bucketName)
    }

    uploadPicture = async (bucketName) => {
        await this.page.waitForSelector("#sidenav > ul > li:nth-child(2) > a")
        let uploadLinkSelector = await this.page.$("#sidenav > ul > li:nth-child(2) > a")
        await uploadLinkSelector.click()
        await this.page.waitForSelector('#bucketselection')
        await this.page.select('#bucketselection', bucketName)
        await this.page.waitForSelector('.fileinput-button input')
        const inputUploadHandle = await this.page.$('.fileinput-button input');
        const filePath = path.relative(process.cwd(), path.join(__dirname, '../inputs/cp.jpeg'));
        await inputUploadHandle.uploadFile(filePath);
        await this.page.waitForSelector('.start button')
        let startUploadingButtonSelector = await this.page.$('.start button')
        await startUploadingButtonSelector.click()
        await this.page.waitForSelector('.delete button')
        expect(true)
    }

    openRecentImage = async () => {
        await this.page.waitForSelector("#sidenav > ul > li:nth-child(3) > a")
        let listImageLinkSelector = await this.page.$("#sidenav > ul > li:nth-child(3) > a")
        await listImageLinkSelector.click()

        await this.page.waitForSelector('#objects li:nth-child(1)')
        let mostRecentImageSelector = await this.page.$('#objects li:nth-child(1)')
        await mostRecentImageSelector.click()
        await this.page.waitForSelector('.span6 div:nth-child(2) a')
    }

    updateImageInfo = async (bucketName) => {
        await this.page.waitForSelector('.span6 div:nth-child(2) a')
        let editButtonSelector = await this.page.$('.span6 div:nth-child(2) a')
        await editButtonSelector.click()
        await this.page.waitForSelector('#id_title')
        let imageTitleField = await this.page.$('#id_title')
        await imageTitleField.type(`Image of testing ${bucketName}`)

        let imageDescField = await this.page.$('#id_description')
        await imageDescField.type(`Image of testing ${bucketName}`)
        let saveEditingButtonSelector = await this.page.$('.form-actions input')
        await saveEditingButtonSelector.click()
        await this.page.waitForSelector('.span6 div:nth-child(2) a')

    }

    validateImageInNewsNow = async (username, password, bucketName) => {
        const newsnowUrl = process.env.ENV==="PROD" ? "https://newsnow.io/story/new" : "https://uat2.dev.newsnow.io/story/new"
        await this.page.goto(newsnowUrl)
        await this.page.waitForSelector("input[name='email']")
        let newsnowUsernameSelector = await this.page.$("input[name='email']")
        await newsnowUsernameSelector.type(username, {delay: 1})
        let newsnowPasswordSelector = await this.page.$("input[name='password']")
        await newsnowPasswordSelector.type(password, {delay: 1})

        await this.page.waitForSelector(".login-button i")
        let newsnowLoginButtonSelector = await this.page.$(".login-button i")
        await newsnowLoginButtonSelector.click()
        /*
        await this.page.waitForSelector('.primary-nav li:nth-child(2) a:nth-child(2)')
        let storyNavSelector = await this.page.$('.primary-nav li:nth-child(2) a:nth-child(2)')
        await storyNavSelector.click()

        await this.page.waitFor(2000)

        await this.page.waitForSelector('.secondary-nav ul li:nth-child(1) a .nav-label')
        let createStorySelector = await this.page.$('.secondary-nav > ul > li:nth-child(1) > a')
        await createStorySelector.click()

        */
        await this.page.waitForSelector('.media-panel', {visible: true})
        let mediaButtonSelector = await this.page.$('.media-panel')
        await mediaButtonSelector.click()

        await this.page.waitForSelector('#storypad')
        const storyPadFrameSelector = await this.page.$('#storypad')
        const storyPadFrame = await storyPadFrameSelector.contentFrame()    

        // await mediaButtonSelector.click()

        await storyPadFrame.waitForSelector('#image-search-box')
        let imageSearchBoxSelector = await storyPadFrame.$('#image-search-box')
        console.log(`\n\n\nImage of testing ${bucketName}\n\n\n`)
        await imageSearchBoxSelector.type(`Image of testing ${bucketName}`, {delay: 1})
        await this.page.waitFor(500)
        await imageSearchBoxSelector.press('Enter')
        await storyPadFrame.waitForSelector('#search-loader')
        await storyPadFrame.waitForSelector('#search-loader', {visible: false})

        await this.page.waitFor(500)
        await storyPadFrame.waitForSelector("#mediaLibraryTab .search-result-wrapper ul li")
        let imageSearchResult = await storyPadFrame.$$("#mediaLibraryTab .search-result-wrapper ul li")
        expect(imageSearchResult.length).to.eql(1)
    }

    openBucket = async (targetBucketName) => {
        await this.page.waitForSelector("#sidenav > ul > li:nth-child(6) > a")
        let bucketListSelector = await this.page.$("#sidenav > ul > li:nth-child(6) > a")
        await bucketListSelector.click()
        await this.page.waitForSelector('table')
        let bucketList = await this.page.$$('tr td:nth-child(1) a')
        for (const bucket of bucketList) {
            const bucketName = await this.page.evaluate(el => el.innerText, bucket);
            if(bucketName === targetBucketName){
                await bucket.click()
                break;
            }
        }
        await this.page.waitForSelector('#bucket_detail')
    }

    validateDeleteBucket = async (isEnabled) => {
        let deleteButton = await this.page.$('.span6 a')
        let deleteButtonClass = await deleteButton.getProperty('className')
        let deleteButtonClassname = await deleteButtonClass.jsonValue()
        console.log(`\n\n\n${deleteButtonClassname}\n\n\n`)
        if(isEnabled){
            expect(deleteButtonClassname).to.not.include('disabled')
            await deleteButton.click()
        } else {
            expect(deleteButtonClassname).to.include('disabled')
        }
    }

    deleteRecentImage = async () => {
        await this.openRecentImage()
        let deleteImageButtonSelector = await this.page.$('.btn-danger')
        await deleteImageButtonSelector.click()
        await this.page.waitForSelector('#object_delete button')
        let confirmDeleteImageButtonSelector = await this.page.$('#object_delete button')
        await confirmDeleteImageButtonSelector.click()
        await this.page.waitForSelector('ul')
    }

}