import puppeteer from 'puppeteer';

const defaultOptions = {
    headless: false,
    slowMo: 0,
    devtools: false
    // slowMo: true
};

export default async (options = undefined) => {
    const puppeterOptions = (options === undefined) ? defaultOptions : options;
    return await puppeteer.launch(puppeterOptions);
};
